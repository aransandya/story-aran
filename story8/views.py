from django.shortcuts import render
from django.http import JsonResponse
import requests
import json

# Create your views here.

def story8(request):
    response = {}
    return render(request, 'story8/booklist.html', response)

def hasil_search(request):
    url = "https://www.googleapis.com/books/v1/volumes?q=" + request.GET['q']
    res = requests.get(url)
    data = json.loads(res.content)
    return JsonResponse(data, safe=False)