$( document ).ready(function() {
    $("#keyword").val("");
    $.ajax({
        method: "GET",
        url: "data?q=day6",
        success: function(data){
            let bookList = $('tbody')
            var arrayBooks = data.items;

            for(i = 0; i < arrayBooks.length; i++){
                var images = arrayBooks[i].volumeInfo.imageLinks.smallThumbnail;
                var titles = arrayBooks[i].volumeInfo.title;
                var authors = arrayBooks[i].volumeInfo.authors;

                if(images == false){
                    var cover = $("<td>").append("-");
                }
                else{
                    var cover = $("<td>").append("<img src=" + images + ">");
                }

                var title = $("<td>").append(titles);

                if(authors == false){
                    var author = $("<td>").append("-");
                }
                else{
                    var author = $("<td>").append(authors);
                }
                var tr = $("<tr>").append(cover, title, author);

                $(bookList).append(tr);
            }
        }
    });


    $("#keyword").keyup(function(){
        var inputan = $("#keyword").val();
        console.log(inputan);

        let bookList = $("tbody");
        if(inputan.length){
            $.ajax({
                url: "data?q=" + inputan,
                success: function(data){
                    var arrayBooks = data.items;
                    console.log(arrayBooks);
                    $(bookList).empty();

                    for(i = 0; i < arrayBooks.length; i++){
                        var images = arrayBooks[i].volumeInfo.imageLinks.smallThumbnail;
                        var titles = arrayBooks[i].volumeInfo.title;
                        var authors = arrayBooks[i].volumeInfo.authors;

                        if(images == false){
                            var cover = $("<td>").append("-");
                        }
                        else{
                            var cover = $("<td>").append("<img src=" + images + ">");
                        }

                        var title = $("<td>").append(titles);

                        if(authors == false){
                            var author = $("<td>").append("-");
                        }
                        else{
                            var author = $("<td>").append(authors);
                        }
                        var tr = $("<tr>").append(cover, title, author);

                        $(bookList).append(tr);
                    }
                }
            });
        };
        $(bookList).empty();
    });
});