from django.test import TestCase, Client
from .views import story8, hasil_search
from django.urls import resolve, reverse
from .apps import Story8Config
from django.apps import apps
import json

# Create your tests here.

class TestURLS(TestCase):
    def test_is_url_story8_available(self):
        response = Client().get('/story-8/')
        self.assertEqual(response.status_code, 200)

class TestHTML(TestCase):
    def test_GET_story8(self):
        response = Client().get('/story-8/')
        self.assertTemplateUsed(response, 'story8/booklist.html')

class TestViews(TestCase):
    def test_views_story8(self):
        found = resolve('/story-8/')
        self.assertEqual(found.func, story8)

    def test_fungsi_data(self):
        response = Client().get('/story-8/data?q=day6')
        self.assertEqual(response.status_code, 200)

class TestApps(TestCase):
    def test_apps(self):
        self.assertEqual(Story8Config.name, 'story8')
        self.assertEqual(apps.get_app_config('story8').name, 'story8')
