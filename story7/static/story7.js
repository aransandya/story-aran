$( function() {
    $( "#acc-activities" ).accordion({
        collapsible: true,
        active: false,
        heightStyle: "panel",
    });
    $( "#acc-experience" ).accordion({
        collapsible: true,
        active: false,
        heightStyle: "panel",
    });
    $( "#acc-achievement" ).accordion({
        collapsible: true,
        active: false,
        heightStyle: "panel",
    });
    $( "#acc-band" ).accordion({
        collapsible: true,
        active: false,
        heightStyle: "panel",
    });
    $( ".button-up" ).click(function(){
        var thisAccor = $(this).parent().parent().parent();
        thisAccor.insertBefore(thisAccor.prev());
        event.stopPropagation(); 
        event.preventDefault(); 
    });
    $( ".button-down" ).click(function(){
        var thisAccor = $(this).parent().parent().parent();
        thisAccor.insertAfter(thisAccor.next());
        event.stopPropagation(); 
        event.preventDefault(); 
    });
} );
