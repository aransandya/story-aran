from django.test import TestCase, Client
from .views import story7
from django.urls import resolve, reverse
from .apps import Story7Config
from django.apps import apps

# Create your tests here.

class TestURLS(TestCase):
    def test_is_url_story7_available(self):
        response = Client().get('/story-7/')
        self.assertEquals(response.status_code, 200)

class TestHTML(TestCase):
    def test_GET_story7(self):
        response = Client().get('/story-7/')
        self.assertTemplateUsed(response, 'story7/storyaccordion.html')

class TestViews(TestCase):
    def test_views_story7(self):
        found = resolve('/story-7/')
        self.assertEqual(found.func, story7)

class TestApps(TestCase):
    def test_apps(self):
        self.assertEqual(Story7Config.name, 'story7')
        self.assertEqual(apps.get_app_config('story7').name, 'story7')
