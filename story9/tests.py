from django.test import TestCase, Client
from django.urls import resolve, reverse
from .apps import Story9Config
from django.apps import apps
from django.contrib.auth.models import User
from .models import Profile
from .forms import FormSignup, FormLogin
from .views import log_in, sign_up, log_out


# Create your tests here.

class TestURLS(TestCase):
    def test_is_url_story9_available(self):
        response = Client().get('/story-9/')
        self.assertEqual(response.status_code, 200)
    
    def test_is_url_signup_available(self):
        response = Client().get('/story-9/sign-up/')
        self.assertEqual(response.status_code, 200)

    def test_is_url_login_available(self):
        response = Client().get('/story-9/log-in/')
        self.assertEqual(response.status_code, 200)
    
    def test_is_url_logout_available(self):
        response = Client().get('/story-9/')
        self.assertEqual(response.status_code, 200)

class TestHTML(TestCase):
    def test_GET_story9_login(self):
        response = Client().get('/story-9/log-in/')
        self.assertTemplateUsed(response, 'story9/login.html')

    def test_GET_story9_signup(self):
        response = Client().get('/story-9/sign-up/')
        self.assertTemplateUsed(response, 'story9/signup.html')

class TestViews(TestCase):
    def setUp(self):
        self.log_in = reverse('story9:login')
        self.sign_up = reverse('story9:signup')
        self.log_out = reverse('story9:logout')
        self.new_user = User.objects.create_user('wonpil','wonpil@jyp.com', password='wonpil123')
        self.new_user.save()
        self.profile = Profile.objects.create(user=self.new_user)
    
    def test_GET_signup(self):
        response = Client().get(self.sign_up, {
            'username':'wonpil',
            'email':'wonpil@jyp.com',
            'password_awal':'wonpil123',
            'password_lagi':'wonpil123',}
            , follow=True
        )
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'story9/signup.html')

    def test_GET_login(self):
        response = Client().get(self.log_in, {
            'username':'wonpil',
            'email':'wonpil@jyp.com',}
            , follow=True
        )
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'story9/login.html')

    def test_GET_logout(self):
        response = Client().get(self.log_out)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'main/home.html')

    def test_signup_login_post(self):
        response = Client().post(self.sign_up, data = {
            'username':'wonpil',
            'email':'wonpil@jyp.com',
            'password_awal':'wonpil123',
            'password_lagi':'wonpil123',
        })

        response = Client().post(self.sign_up, data = {
            'username':'wonpil',
            'email':'wonpil@jyp.com',
            'password_awal':'wonpil123',
            'password_lagi':'wonpil',
        })

        response = Client().post(self.sign_up, data = {
            'username':'wooonpil',
            'email':'wonpil123@jyp.com',
            'password_awal':'wonpil123',
            'password_lagi':'wonpil123',
        })

        response = Client().post(self.log_in, data = {
            'username':'wonpil',
            'password':'wonpil123',
        })
        self.assertEquals(response.status_code, 302)
    
    def test_not_registered(self):
        response = Client().post(self.log_in, data={
            'username':'dowoon',
            'password':'dowoon123'
        })
        self.assertEqual(response.status_code, 200)

class TestModel(TestCase):
    def setUp(self):
        self.new_user = User.objects.create_user('wonpil', password='wonpil123')
        self.new_user.save()
        self.profile = Profile.objects.create(
            user = self.new_user
        )
        self.response = Client().login(
            username = 'wonpil',
            password = 'wonpil123'
        )
    
    def test_instance_created(self):
        self.assertEqual(Profile.objects.count(), 1)

    def test_instance_is_correct(self):
        self.assertEqual(Profile.objects.first().user, self.new_user)

    def test_str(self):
        self.assertIn('wonpil', str(self.new_user.profile))

class TestForm(TestCase):
    def test_form_is_valid(self):
        form_signup = FormSignup(data={
            'username':'wonpil',
            'email':'wonpil@jyp.com',
            'password_awal':'wonpil123',
            'password_lagi':'wonpil123',
        })

        form_login = FormLogin(data={
            'username':'wonpil',
            'password':'wonpil123'
        })
        self.assertTrue(form_signup.is_valid())
    
    def test_form_is_invalid(self):
        form_signup = FormSignup(data={})
        self.assertFalse(form_signup.is_valid())

        form_login = FormLogin(data={})
        self.assertFalse(form_login.is_valid())
    
    # def test_form_signup_exist(self):
    #     form_signup = FormSignup(data={
    #         'username':'wonpil',
    #         'email':'wonpil@jyp.com',
    #         'password_awal':'wonpil123',
    #         'password_lagi':'wonpil123',
    #     })
    #     self.assertTrue

class TestApps(TestCase):
    def test_apps(self):
        self.assertEqual(Story9Config.name, 'story9')
        self.assertEqual(apps.get_app_config('story9').name, 'story9')
