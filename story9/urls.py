from django.urls import path

from . import views

app_name = 'story9'

urlpatterns = [
    path('sign-up/', views.sign_up, name='signup'),
    path('log-in/', views.log_in, name='login'),
    path('', views.log_out, name='logout'),
]
