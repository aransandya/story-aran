from django import forms
from .models import Profile
from django.contrib.auth import get_user_model

User = get_user_model()

class FormLogin(forms.Form):
    username = forms.CharField(widget=forms.TextInput(attrs={'style':'width:100%; border-radius:5px; border:none'}))
    password = forms.CharField(widget=forms.PasswordInput(attrs={'style':'width:100%; border-radius:5px; border:none'}))

class FormSignup(forms.Form):
    username = forms.CharField(widget=forms.TextInput(attrs={'style':'width:100%; border-radius:5px; border:none'}), label="Username:")
    email = forms.EmailField(widget=forms.EmailInput(attrs={'style':'width:100%; border-radius:5px; border:none'}), label="E-mail:")
    password_awal = forms.CharField(label="Password:", widget=forms.PasswordInput(attrs={'style':'width:100%; border-radius:5px; border:none'}))
    password_lagi = forms.CharField(label="Confirm password:", widget=forms.PasswordInput(attrs={'style':'width:100%; border-radius:5px; border:none'}))

    def clean(self):
        cleaned_data = self.cleaned_data
        password_satu = self.cleaned_data.get('password_awal')
        password_dua = self.cleaned_data.get('password_lagi')
        if password_satu != password_dua:
            raise forms.ValidationError("Passwords don't match")
        return cleaned_data

    def clean_username(self):
        username = self.cleaned_data.get('username')
        user_by_name = User.objects.filter(username=username)
        if user_by_name.exists():
            raise forms.ValidationError("The username you've chosen is unavailable.")
        return username

    def clean_email(self):
        email_address = self.cleaned_data.get('email')
        user_by_email = User.objects.filter(email=email_address)
        if user_by_email.exists():
            raise forms.ValidationError("The email address you've chosen is already registered.")
        return email_address

