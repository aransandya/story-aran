from django.shortcuts import render, redirect, HttpResponseRedirect
from django.contrib.auth import authenticate, login, logout, get_user_model
from django.contrib import messages
from .models import Profile
from .forms import FormLogin, FormSignup

User = get_user_model()

def sign_up(request):
    form_signup = FormSignup(request.POST or None)
    context = {
        'form_signup': form_signup,
    }
    if form_signup.is_valid():
        username = form_signup.cleaned_data.get('username')
        email = form_signup.cleaned_data.get('email')
        password = form_signup.cleaned_data.get('password_awal')
        User.objects.create_user(username, email, password)

        return HttpResponseRedirect('/story-9/log-in')
    return render(request, 'story9/signup.html', context=context)

def log_in(request):
    form_login = FormLogin(request.POST or None)
    context = {
        'form_login': form_login
    }
    print(request.user.is_authenticated)
    if form_login.is_valid():
        print(form_login.cleaned_data)
        username = form_login.cleaned_data.get('username')
        password = form_login.cleaned_data.get('password')
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            return redirect('/')
        else:
            print("error.......")

    return render(request, 'story9/login.html', context=context)

def log_out(request):
    logout(request)
    return render(request, 'main/home.html')
